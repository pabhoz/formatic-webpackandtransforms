import './main.scss';
import './app/index';

import './app/user/user.html';

import _img1 from './assets/01.jpg';
import _img2 from './assets/02.jpg';
import _img3 from './assets/03.jpg';
 
let images = [_img1,_img2,_img3];
for (let i = 0; i < images.length; i++) {
    var el = document.getElementById(`img${i+1}`);
    if(el != null){
        el.src=`./${images[i]}`;
    }
}